# A simple Rails validator for validating a string is actually a URL.
#
# In the model, use...
#   validates :url, :presence => true, :url => true
class UrlValidator < ActiveModel::EachValidator

  def validate_each(record, attribute, value)
    valid = UrlValidator.is_valid?(value)
    unless valid
      record.errors[attribute] << (options[:message] || I18n.t('errors.messages.url'))
    end
  end

  def self.is_valid?(value)
    begin
      URI.parse(value).kind_of?(URI::HTTP)
    rescue URI::InvalidURIError
      false
    end
  end

end
