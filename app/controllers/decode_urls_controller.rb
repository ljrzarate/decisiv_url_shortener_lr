class DecodeUrlsController < ApplicationController

  def new
  end

  def create
    load_url
    redirection_to_url
  end

  private

  def load_url
    @url ||= url_scope.with_token(token_from_url)
  end

  def url_scope
    Url.all
  end

  def permitted_params
    params.require(:url).permit(:short_url)
  end

  def token_from_url
    permitted_params[:short_url].split("/").last
  end

  def redirection_to_url
    if !@url.present?
      flash[:error] = t("errors.alert.not_able_to_decode")
      redirect_to action: :new
    else
      flash[:notice] = "Success!"
      redirect_to url_path(@url.token, redirect: "no")
    end
  end

end
