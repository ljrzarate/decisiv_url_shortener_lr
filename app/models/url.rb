class Url < ActiveRecord::Base
  attr_accessor :short_url

  TOKEN_LENGTH = 7
  TOKEN_ALPHABET = %w{ 2 3 4 6 7 9 A C E F G H J K M N P Q R T X Y Z }

  validates :token, presence: true, uniqueness: true
  validates :long_url, presence: true, url: true

  before_validation :set_token, on: :create

  def to_param
    token
  end

  def self.with_token(token)
    where(token: token).first
  end

  private

  def set_token
    self.token = make_unique_token
  end

  def make_unique_token
    loop do
      token = build_token_value
      return token if token_is_unique?(token)
    end
  end

  def token_is_unique?(token)
    self.class.where(token: token).count == 0
  end

  def build_token_value
    (0...TOKEN_LENGTH).map { TOKEN_ALPHABET.to_a[rand(TOKEN_ALPHABET.size)] }.join
  end

end
