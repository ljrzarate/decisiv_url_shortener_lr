# Decisiv URL Shortener

The Decisiv URL Shortener is a web application that provides two primary 
features:

1. Given a URL, it can generate a short URL
2. Given a previously-generated short URL, it can redirect you to the 
corresponding original long URL. 

Short URLs are based on a unique token, seven characters in length, consisting only of numbers and upper-case letters, excluding ambiguous characters (`1`/`I`/`L`, `5`/`S`, `0`/`O`/`D`, `8`/`B`, `U`/`V`/`W`).

## Setup

Built using Rails 4.2.5 and Ruby 2.1.7.  There’s a full RSpec/Capybara test suite in place, simple error handling (404/500 pages), etc.  The front-end is Bootstrap 3.  It runs against SQLite (`sqlite3`) in the dev & test environments.  It has a `.ruby-version` file to get up and running on rvm/rbenv/chruby.

Running locally should be as simple as cloning this repository, install Bundler (if not already available), bundle, migrate, and run:

```
gem install bundler
bundle install
bin/rake db:migrate
bin/rails server
```

... then hit http://localhost:3000 to see it in action.

Capybara is by default configured to use the headless WebKit driver, but feel free to replace this with Selenium or Poltergeist.  See the [Gemfile](Gemfile) and [Capybara configuration file](spec/support/config/capybara.rb) for details on how to get the different drivers up and running.

## Testing

```
bin/rake spec
```

(One thing to note, you can’t do `js: true` feature specs with SQLite without throwing `SQLite3::BusyException - database is locked exceptions`, but there’s no JS in the front-end anyway.  The benefit of not requiring the candidate to have PostgreSQL or MySQL running locally should outweigh this problem.)
