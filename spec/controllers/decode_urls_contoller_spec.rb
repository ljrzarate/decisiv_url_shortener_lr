require 'rails_helper'

RSpec.describe DecodeUrlsController, type: :controller do

  describe "parameter whitelisting" do
    let(:params) { {other: "stuff", url: {token: "ABC1234", long_url: "http://example.org"}} }
    it { is_expected.to permit(:short_url).for(:create, params: params).on(:url) }
  end

  describe "GET #new" do
    subject(:action) { get :new }

    before do
      action
    end

    it "returns a HTTP 200 (success) status" do
      expect(action).to have_http_status :success
    end

    it "renders the :new template" do
      expect(action).to render_template :new
    end
  end

  describe "POST #create" do
    let(:original_url) { "http://this.url.is.very.long.com/some/path" }
    before do
      @url = Url.create(long_url: original_url)
    end

    context "with valid attributes" do
      it "redirects to the #show page with redirect=false" do
        params = {url: {short_url: "http://test.host/#{@url.token}"}}
        post :create, params
        expect(response).to redirect_to "/urls/#{@url.token}?redirect=no"
      end

      it "sets the success flash message" do
        params = {url: {short_url: "http://test.host/#{@url.token}"}}
        post :create, params
        expect(flash[:notice]).to be_present
      end
    end

    context "with invalid shor url" do
      let(:original_url) { "invalid.url" }

      it "redirects to the #new page with error message" do
        params = {url: {short_url: "http://test.host/#{@url.token}-No-URL"}}
        post :create, params
        expect(response).to redirect_to action: :new
      end
    end
  end

end
